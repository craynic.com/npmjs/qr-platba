import {QrRequest} from "../src"
import {QrRequestRenderer, QrCodeSVG} from "../src/QrRequestRenderer"

describe('QrRequestRenderer tests', (): void => {
    test('The render method produces data URL', async (): Promise<void> => {
        const qrRequest: QrRequest = new QrRequest('CZ6508000000192000145399'),
            qrRequestRenderer: QrRequestRenderer = new QrRequestRenderer(),
            renderedSVG: QrCodeSVG = await qrRequestRenderer.toSVG(qrRequest)

        expect(renderedSVG.contents).toContain('<svg')
        expect(renderedSVG.size).toBeGreaterThanOrEqual(1)
        expect(renderedSVG.size).toBeLessThanOrEqual(40)
    })
})
