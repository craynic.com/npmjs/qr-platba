import {sanitizeQrString} from "../src/sanitizeQrString"

describe('sanitizeQrString tests', (): void => {
    test('sanitizeQrString keeps string untouched if it does not contain special characters', (): void => {
        const value: string = 'this is a regular string'

        expect(sanitizeQrString(value)).toBe(value)
    })

    test('sanitizeQrString replaces special characters', (): void => {
        const value: string = 'this is a regular string with irregular*characters',
            expectedValue: string = 'this is a regular string with irregular%2Acharacters'

        expect(sanitizeQrString(value)).toBe(expectedValue)
    })

    test('sanitizeQrString accepts object and transfers it to string', (): void => {
        const value: string = 'this is not a regular string',
            objectValue: object = {
            toString: (): string => value
        }

        expect(sanitizeQrString(objectValue)).toBe(value)
    })

    test('sanitizeQrString accepts object, transfers it to string and replaces special characters', (): void => {
        const value: string = 'this is a regular string with irregular*characters',
            expectedValue: string = 'this is a regular string with irregular%2Acharacters',
            objectValue: object = {
                toString: (): string => value
            }

        expect(sanitizeQrString(objectValue)).toBe(expectedValue)
    })
})
