import {QrRequest} from "../src"
import * as Dinero from "dinero.js"
import {RequestType} from "../src/QrRequest";
import {QrRequestDate} from "../src/QrRequestDate";

describe('QrRequest tests', (): void => {
    const validIBAN: string = 'CZ6508000000192000145399',
        invalidIBAN: string = 'CZ5508000000192000145399',
        validIBANBIC: string = 'CZ6508000000192000145399+GIBACZPX',
        invalidIBANBIC: string = 'CZ6508000000192000145399*GIBACZPX'

    test('QrRequest constructor works with IBAN', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect(String(qrRequest)).toBe(`SPD*1.2*ACC:${validIBAN}*CRC32:${qrRequest.getChecksum()}`)
    })

    test('QrRequest constructor works with IBAN and BIC', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBANBIC)

        expect(String(qrRequest)).toBe(`SPD*1.2*ACC:${validIBANBIC}*CRC32:${qrRequest.getChecksum()}`)
    })

    test('QrRequest constructor works with multiple accounts with IBANs', (): void => {
        const qrRequest: QrRequest = new QrRequest([validIBAN, validIBANBIC])

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*ALT-ACC:${validIBANBIC}*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test('QrRequest constructor works with multiple accounts with IBANs and BICs', (): void => {
        const qrRequest: QrRequest = new QrRequest([validIBANBIC, validIBANBIC, validIBANBIC])

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBANBIC}*ALT-ACC:${validIBANBIC},` +
            `${validIBANBIC}*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [[invalidIBAN], [invalidIBANBIC]]
    )('QrRequest constructor fails with invalid bank account (%s)', (invalidBankAccount: string): void => {
        expect((): void => {
            new QrRequest(invalidBankAccount)
        }).toThrowError()
    })

    test('QrRequest constructor fails with too many bank accounts', (): void => {
        expect((): void => {
            new QrRequest([validIBAN, validIBAN, validIBAN, validIBAN])
        }).toThrowError()
    })

    test('QrRequest constructor fails with too few bank accounts', (): void => {
        expect((): void => {
            new QrRequest([])
        }).toThrowError()
    })

    test('QrRequest constructor can construct collection request', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN, RequestType.CollectionRequest);

        expect(String(qrRequest)).toBe('SCD*1.2*ACC:CZ6508000000192000145399*CRC32:06C8A9BC');
    })

    test('QrRequest account can be changed to a single one', (): void => {
        const qrRequest: QrRequest = new QrRequest([validIBAN, validIBAN, validIBAN]),
            modQrRequest: QrRequest = qrRequest.setAccount(validIBANBIC)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBANBIC}*CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getAccount()).toEqual([validIBAN, validIBAN, validIBAN])
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest account can be changed to a new set', (): void => {
        const qrRequest: QrRequest = new QrRequest([validIBAN, validIBAN, validIBAN]),
            modQrRequest: QrRequest = qrRequest.setAccount([validIBAN, validIBANBIC])

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*ALT-ACC:${validIBANBIC}*CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getAccount()).toEqual([validIBAN, validIBAN, validIBAN])
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each<[number|null]>(
        [[200], [null]]
    )('QrRequest amount can be changed to a new value (%s)', (amount: number|null): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setAmountInCents(100),
            modQrRequest: QrRequest = qrRequest.setAmountInCents(amount)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}${amount === null ? '' : `*AM:2.00*CC:CZK`}`
            + `*CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getAmount()?.equalsTo(Dinero({amount: 100, currency: 'CZK'}))).toBeTruthy()
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest allows to set maximum amount and defaults to CZK', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setAmountInCents(9_999_999_99)

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*AM:9999999.99*CC:CZK*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test('QrRequest fails with amount too high', (): void => {
        expect((): void => {
            new QrRequest(validIBAN).setAmountInCents(10_000_000_00)
        }).toThrowError()
    })

    test('QrRequest constructor fails with amount too low', (): void => {
        expect((): void => {
            new QrRequest(validIBAN).setAmountInCents(0)
        }).toThrowError()
    })

    test.each(
        [['new message'], [null]]
    )('QrRequest allows to change the message (%s)', (newMessage: string|null): void => {
        const oldValue: string = 'old message',
            qrRequest: QrRequest = new QrRequest(validIBAN).setMessage(oldValue),
            modQrRequest: QrRequest = qrRequest.setMessage(newMessage)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newMessage === null ? '' : `MSG:${newMessage}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getMessage()).toBe(oldValue)
        expect(modQrRequest.getMessage()).toBe(newMessage)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each(
        [['abcdefghijklmnopqrstuvwxyz-abcdefghijklmnopqrstuvwxyz-abcdefg'], ['*********************']]
    )('QrRequest fails with message too long (%s)', (tooLongMessage: string): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect((): void => {
            qrRequest.setMessage(tooLongMessage)
        }).toThrowError()
    })

    test('QrRequest enables to set the variable symbol', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setVariableSymbol('1234567890')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-VS:1234567890*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['0000000000'], [null]]
    )('QrRequest allows to change the variable symbol (%s)', (newSymbol: string|null): void => {
        const oldValue: string = '1111111111',
            qrRequest: QrRequest = new QrRequest(validIBAN).setVariableSymbol(oldValue),
            modQrRequest: QrRequest = qrRequest.setVariableSymbol(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `X-VS:${newSymbol}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getVariableSymbol()).toBe(oldValue)
        expect(modQrRequest.getVariableSymbol()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each(
        [['123A'], ['123456789A'], ['12345678901']]
    )('QrRequest fails with invalid variable symbol (%s)', (invalidValue: string): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect((): void => {
            qrRequest.setVariableSymbol(invalidValue)
        }).toThrowError()
    })

    test('QrRequest enables to set the specific symbol', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setSpecificSymbol('1234567890')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-SS:1234567890*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['0000000000'], [null]]
    )('QrRequest allows to change the specific symbol (%s)', (newSymbol: string|null): void => {
        const oldValue: string = '1111111111',
            qrRequest: QrRequest = new QrRequest(validIBAN).setSpecificSymbol(oldValue),
            modQrRequest: QrRequest = qrRequest.setSpecificSymbol(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `X-SS:${newSymbol}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getSpecificSymbol()).toBe(oldValue)
        expect(modQrRequest.getSpecificSymbol()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each(
        [['123A'], ['123456789A'], ['12345678901']]
    )('QrRequest fails with invalid specific symbol (%s)', (invalidValue: string): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect((): void => {
            qrRequest.setSpecificSymbol(invalidValue)
        }).toThrowError()
    })

    test('QrRequest enables to set the constant symbol', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setConstantSymbol('1234567890')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-KS:1234567890*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['0000000000'], [null]]
    )('QrRequest allows to change the constant symbol (%s)', (newSymbol: string|null): void => {
        const oldValue: string = '1111111111',
            qrRequest: QrRequest = new QrRequest(validIBAN).setConstantSymbol(oldValue),
            modQrRequest: QrRequest = qrRequest.setConstantSymbol(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `X-KS:${newSymbol}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getConstantSymbol()).toBe(oldValue)
        expect(modQrRequest.getConstantSymbol()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each(
        [['123A'], ['123456789A'], ['12345678901']]
    )('QrRequest fails with invalid constant symbol (%s)', (invalidValue: string): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect((): void => {
            qrRequest.setConstantSymbol(invalidValue)
        }).toThrowError()
    })

    test('QrRequest enables to set the payment reference', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setPaymentReference('1234567890123456')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*RF:1234567890123456*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['0000000000'], [null]]
    )('QrRequest allows to change the payment reference (%s)', (newSymbol: string|null): void => {
        const oldValue: string = '1111111111',
            qrRequest: QrRequest = new QrRequest(validIBAN).setPaymentReference(oldValue),
            modQrRequest: QrRequest = qrRequest.setPaymentReference(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `RF:${newSymbol}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getPaymentReference()).toBe(oldValue)
        expect(modQrRequest.getPaymentReference()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each(
        [['123A'], ['123456789A'], ['12345678901234567']]
    )('QrRequest fails with invalid payment reference (%s)', (invalidValue: string): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect((): void => {
            qrRequest.setPaymentReference(invalidValue)
        }).toThrowError()
    })

    test('QrRequest enables to set the receiver name', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setReceiverName(
            'abcdefghijklmnopqrstuvwxyz-abcdefgh'
        )

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*RN:abcdefghijklmnopqrstuvwxyz-abcdefgh`
            + `*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['0000000000'], [null]]
    )('QrRequest allows to change the receiver name (%s)', (newSymbol: string|null): void => {
        const oldValue: string = 'some name here',
            qrRequest: QrRequest = new QrRequest(validIBAN).setReceiverName(oldValue),
            modQrRequest: QrRequest = qrRequest.setReceiverName(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `RN:${newSymbol}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getReceiverName()).toBe(oldValue)
        expect(modQrRequest.getReceiverName()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest fails with too long receiver name', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)

        expect((): void => {
            qrRequest.setPaymentReference('abcdefghijklmnopqrstuvwxyz-abcdefghi')
        }).toThrowError()
    })

    test('QrRequest enables to set the date of transaction', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setDateOfTransaction(
            new Date('2022-12-31 23:59:59')
        )

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*DT:20221231*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [[new Date('2014-11-10 11:30:00')], [new Date('2020-12-10 14:11')], [null]]
    )('QrRequest allows to change the date of transaction (%s)', (newSymbol: Date|null): void => {
        const oldValue: Date = new Date('2022-12-31 23:59:59'),
            qrRequest: QrRequest = new QrRequest(validIBAN).setDateOfTransaction(oldValue),
            modQrRequest: QrRequest = qrRequest.setDateOfTransaction(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `DT:${new QrRequestDate(newSymbol)}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getDateOfTransaction()).toBe(oldValue)
        expect(modQrRequest.getDateOfTransaction()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest enables to set the date of last transaction', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setDateOfLastTransaction(
            new Date('2022-12-31 23:59:59')
        )

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*DL:20221231*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [[new Date('2014-11-10 11:30:00')], [new Date('2020-12-10 14:11')], [null]]
    )('QrRequest allows to change the date of the last transaction (%s)', (newSymbol: Date|null): void => {
        const oldValue: Date = new Date('2022-12-31 23:59:59'),
            qrRequest: QrRequest = new QrRequest(validIBAN).setDateOfLastTransaction(oldValue),
            modQrRequest: QrRequest = qrRequest.setDateOfLastTransaction(newSymbol)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newSymbol === null ? '' : `DL:${new QrRequestDate(newSymbol)}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getDateOfLastTransaction()).toBe(oldValue)
        expect(modQrRequest.getDateOfLastTransaction()).toBe(newSymbol)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest enables to set the notification type', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setNotificationType('P')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*NT:P*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each<['E']|[null]>(
        [['E'], [null]]
    )('QrRequest allows to change the notification name (%s)', (newValue: 'P'|'E'|null): void => {
        const oldValue: 'P' = 'P',
            qrRequest: QrRequest = new QrRequest(validIBAN).setNotificationType(oldValue),
            modQrRequest: QrRequest = qrRequest.setNotificationType(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === null ? '' : `NT:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getNotificationType()).toBe(oldValue)
        expect(modQrRequest.getNotificationType()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest enables to set the notification address', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setNotificationAddress('email@example.com')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*NTA:email@example.com*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['email2@example.net'], [null]]
    )('QrRequest allows to change the notification address (%s)', (newValue: string|null): void => {
        const oldValue: string = 'email@example.com',
            qrRequest: QrRequest = new QrRequest(validIBAN).setNotificationAddress(oldValue),
            modQrRequest: QrRequest = qrRequest.setNotificationAddress(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === null ? '' : `NTA:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getNotificationAddress()).toBe(oldValue)
        expect(modQrRequest.getNotificationAddress()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest fails with too long notification address', (): void => {
        expect((): void => {
            new QrRequest(validIBAN).setNotificationAddress('X'.repeat(321))
        }).toThrowError()
    })

    test('QrRequest enables to set the frequency', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setFrequency('12D')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*FRQ:12D*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['24M'], ['1D'], ['3Y'], [null]]
    )('QrRequest allows to change the frequency (%s)', (newValue: string|null): void => {
        const oldValue: string = '12D',
            qrRequest: QrRequest = new QrRequest(validIBAN).setFrequency(oldValue),
            modQrRequest: QrRequest = qrRequest.setFrequency(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === null ? '' : `FRQ:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getFrequency()).toBe(oldValue)
        expect(modQrRequest.getFrequency()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each(
        [['999M'], ['999'], ['Y3'], [''], ['ABC'], ['DMY']]
    )('QrRequest fails on invalid frequency (%s)', (invalidValue: string): void => {
        expect((): void => {
            new QrRequest(validIBAN).setFrequency(invalidValue)
        }).toThrowError()
    })

    test('QrRequest enables to set payment after death', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setPayAfterDeath(true)

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*DH:1*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test('QrRequest allows to change the payment after death', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setPayAfterDeath(false),
            modQrRequest: QrRequest = qrRequest.setPayAfterDeath(true)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*DH:1*CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.isPayAfterDeath()).toBeFalsy()
        expect(modQrRequest.isPayAfterDeath()).toBeTruthy()
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest enables to set the retry period', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setRetryPeriod(15)

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-PER:15*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [[1], [10], [30], [null]]
    )('QrRequest allows to change the retry period (%d)', (newValue: number|null): void => {
        const oldValue: number = 12,
            qrRequest: QrRequest = new QrRequest(validIBAN).setRetryPeriod(oldValue),
            modQrRequest: QrRequest = qrRequest.setRetryPeriod(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === null ? '' : `X-PER:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getRetryPeriod()).toBe(oldValue)
        expect(modQrRequest.getRetryPeriod()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test.each([[-1], [31]])('QrRequest fails on invalid retry period (%d)', (invalidValue: number): void => {
        expect((): void => {
            new QrRequest(validIBAN).setRetryPeriod(invalidValue)
        }).toThrowError()
    })

    test('QrRequest enables to set the sender ID', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setSenderID('sender id')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-ID:sender id*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['new sender reference'], ['X'.repeat(20)], [null]]
    )('QrRequest allows to change the sender ID (%s)', (newValue: string|null): void => {
        const oldValue: string = 'sender id',
            qrRequest: QrRequest = new QrRequest(validIBAN).setSenderID(oldValue),
            modQrRequest: QrRequest = qrRequest.setSenderID(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === null ? '' : `X-ID:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getSenderID()).toBe(oldValue)
        expect(modQrRequest.getSenderID()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest fails on too long sender ID', (): void => {
        expect((): void => {
            new QrRequest(validIBAN).setSenderID('X'.repeat(21))
        }).toThrowError()
    })

    test('QrRequest enables to set the URL', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setURL('https://www.example.com')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-URL:https://www.example.com*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [['//www.example.net'], ['X'.repeat(140)], [''], [null]]
    )('QrRequest allows to change the URL (%s)', (newValue: string|null): void => {
        const oldValue: string = 'https://www.example.com',
            qrRequest: QrRequest = new QrRequest(validIBAN).setURL(oldValue),
            modQrRequest: QrRequest = qrRequest.setURL(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === '' || newValue === null ? '' : `X-URL:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getURL()).toBe(oldValue)
        expect(modQrRequest.getURL()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest fails on too long URL', (): void => {
        expect((): void => {
            new QrRequest(validIBAN).setFrequency('X'.repeat(141))
        }).toThrowError()
    })

    test('QrRequest enables to set the sender note', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN).setSenderNote('sender note')

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*X-SELF:sender note*CRC32:${qrRequest.getChecksum()}`
        )
    })

    test.each(
        [[''], ['X'.repeat(60)], [null]]
    )('QrRequest allows to change the sender note (%s)', (newValue: string|null): void => {
        const oldValue: string = 'sender note',
            qrRequest: QrRequest = new QrRequest(validIBAN).setSenderNote(oldValue),
            modQrRequest: QrRequest = qrRequest.setSenderNote(newValue)

        expect(String(modQrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*${
                newValue === null || newValue === '' ? '' : `X-SELF:${newValue}*`
            }CRC32:${modQrRequest.getChecksum()}`
        )
        expect(qrRequest.getSenderNote()).toBe(oldValue)
        expect(modQrRequest.getSenderNote()).toBe(newValue)
        expect(qrRequest).not.toBe(modQrRequest)
    })

    test('QrRequest fails on too long sender note', (): void => {
        expect((): void => {
            new QrRequest(validIBAN).setSenderNote('X'.repeat(61))
        }).toThrowError()
    })

    test('QrRequest all getters & setters work', (): void => {
        const qrRequest: QrRequest = new QrRequest([validIBAN, validIBANBIC]).setAmountInCents(123_456_789)
                .setMessage('The message!')
                .setConstantSymbol('0308')
                .setSpecificSymbol('9999')
                .setVariableSymbol('1234567890')
                .setPaymentReference('0987654321')
                .setDateOfTransaction(new Date('2022-12-31 23:59:59'))
                .setPaymentType('ABC')
                .setNotificationType('E')
                .setNotificationAddress('email@example.com')
                .setDateOfLastTransaction(new Date('2023-12-31 23:59:59'))
                .setFrequency('12D')
                .setPayAfterDeath(true)
                .setRetryPeriod(15)
                .setSenderID('sender id here')
                .setURL('https://www.example.com')
                .setSenderNote("sender's private note here"),
            expectedChecksum: string = '6F3BBCB4'

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*ALT-ACC:CZ6508000000192000145399+GIBACZPX*AM:1234567.89*CC:CZK*`
            + `DH:1*DL:20231231*DT:20221231*FRQ:12D*MSG:The message!*NT:E*NTA:email@example.com*PT:ABC*RF:0987654321*`
            + `X-ID:sender id here*X-KS:0308*X-PER:15*X-SELF:sender's private note here*X-SS:9999*`
            + `X-URL:https://www.example.com*X-VS:1234567890*CRC32:${expectedChecksum}`
        )

        expect(qrRequest.getAccount()).toEqual([validIBAN, validIBANBIC])
        expect(qrRequest.getAmount()?.equalsTo(Dinero({amount: 123456789, currency: 'CZK'}))).toBeTruthy()
        expect(qrRequest.getMessage()).toBe('The message!')
        expect(qrRequest.getConstantSymbol()).toBe('0308')
        expect(qrRequest.getSpecificSymbol()).toBe('9999')
        expect(qrRequest.getVariableSymbol()).toBe('1234567890')
        expect(qrRequest.getPaymentReference()).toBe('0987654321')
        expect(qrRequest.getDateOfTransaction()).toEqual(new Date('2022-12-31 23:59:59'))
        expect(qrRequest.getDateOfLastTransaction()).toEqual(new Date('2023-12-31 23:59:59'))
        expect(qrRequest.getPaymentType()).toBe('ABC')
        expect(qrRequest.getNotificationType()).toBe('E')
        expect(qrRequest.getNotificationAddress()).toBe('email@example.com')
        expect(qrRequest.getFrequency()).toBe('12D')
        expect(qrRequest.isPayAfterDeath()).toBeTruthy()
        expect(qrRequest.getRetryPeriod()).toBe(15)
        expect(qrRequest.getSenderID()).toBe('sender id here')
        expect(qrRequest.getURL()).toBe('https://www.example.com')
        expect(qrRequest.getSenderNote()).toBe("sender's private note here")
        expect(qrRequest.getChecksum()).toBe(expectedChecksum)
    })

    test('QrRequest empty values are accepted but not rendered', (): void => {
        const qrRequest: QrRequest = new QrRequest([validIBAN, validIBANBIC]).setAmountInCents(123_456_789)
                .setMessage('')
                .setConstantSymbol('')
                .setSpecificSymbol('')
                .setVariableSymbol('')
                .setPaymentReference('')
                .setPaymentType('')
                .setReceiverName('')
                .setNotificationAddress('')
                .setSenderID('')
                .setURL('')
                .setSenderNote('');

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*ALT-ACC:CZ6508000000192000145399+GIBACZPX*AM:1234567.89*CC:CZK`
            + `*CRC32:${qrRequest.getChecksum()}`
        )

        expect(qrRequest.getAccount()).toEqual([validIBAN, validIBANBIC])
        expect(qrRequest.getAmount()?.equalsTo(Dinero({amount: 123456789, currency: 'CZK'}))).toBeTruthy()
        expect(qrRequest.getMessage()).toBe('')
        expect(qrRequest.getConstantSymbol()).toBe('')
        expect(qrRequest.getSpecificSymbol()).toBe('')
        expect(qrRequest.getVariableSymbol()).toBe('')
        expect(qrRequest.getPaymentReference()).toBe('')
        expect(qrRequest.getPaymentType()).toBe('')
        expect(qrRequest.getReceiverName()).toBe('')
        expect(qrRequest.getNotificationAddress()).toBe('')
        expect(qrRequest.getSenderID()).toBe('')
        expect(qrRequest.getURL()).toBe('')
        expect(qrRequest.getSenderNote()).toBe('')
    })

    test('QrRequest checksum is always exactly 8 digits long', (): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)
            .setAmountInCents(32)

        expect(String(qrRequest)).toBe(
            `SPD*1.2*ACC:${validIBAN}*AM:0.32*CC:CZK*CRC32:0EBB5FE9`
        )
    })

    test.each<[string, boolean]>([
        ['0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:', true],
        ['^', false],
        ['', true],
    ])('QrRequest correctly indicates whether it\'s alphanumeric', (message: string, result: boolean): void => {
        const qrRequest: QrRequest = new QrRequest(validIBAN)
            .setMessage(message)

        expect(qrRequest.isAlphanumericQRCode()).toBe(result)
    })
})
