import {QrRequestDate} from "../src/QrRequestDate"

describe('QrRequestDate tests', (): void => {
    test('The format function works correctly', (): void => {
        const qrRequestDate: QrRequestDate = new QrRequestDate(new Date('2022-12-31 23:59:59'))

        expect(String(qrRequestDate)).toBe('20221231')
    })
})
