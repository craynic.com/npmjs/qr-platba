export function sanitizeQrString(value: string | object): string {
    return /\*/g[Symbol.replace](String(value), '%2A')
}
