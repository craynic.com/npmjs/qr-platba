export class QrRequestDate {
    public constructor(private _date: Date) {}

    public get date(): Date {
        return this._date
    }

    toString(): string {
        return `${this._date.getFullYear()}${String(this._date.getMonth() + 1).padStart(2, '0')}${String(
            this._date.getDate(),
        ).padStart(2, '0')}`
    }
}
