import { QrRequest } from './QrRequest'
import { create, QRCode, QRCodeToStringOptions, toString } from 'qrcode'

export type QrCodeSVG = {
    size: number
    contents: string
}

export class QrRequestRenderer {
    public toSVG(qrRequest: QrRequest): Promise<QrCodeSVG> {
        const qrCodeOptions: QRCodeToStringOptions = {
                type: 'svg',
                margin: 4,
                errorCorrectionLevel: 'M',
            },
            qrCode: QRCode = create(String(qrRequest), qrCodeOptions)

        return toString(String(qrRequest), qrCodeOptions).then((contents: string): QrCodeSVG => {
            return {
                size: qrCode.modules.size,
                contents: contents,
            }
        })
    }
}
