import { isValidBIC, isValidIBAN } from 'ibantools'
import { crc32 } from 'crc'
import * as Dinero from 'dinero.js'
import { cloneDeep } from 'lodash'
import { QrRequestDate } from './QrRequestDate'
import { sanitizeQrString } from './sanitizeQrString'

export enum RequestType {
    PaymentRequest = 'SPD',
    CollectionRequest = 'SCD',
}

export class QrRequest {
    public static readonly VERSION: string = '1.2'

    // noinspection TypeScriptFieldCanBeMadeReadonly
    private account: BankAccountString[]
    // noinspection TypeScriptFieldCanBeMadeReadonly
    private amountInCents: AmountInCents | null = null

    private message: Message | null = null
    private constantSymbol: ConstantSymbol | null = null
    private specificSymbol: SpecificSymbol | null = null
    private variableSymbol: VariableSymbol | null = null

    private paymentReference: PaymentReference | null = null
    private receiverName: ReceiverName | null = null

    private dateOfTransaction: QrRequestDate | null = null
    private dateOfLastTransaction: QrRequestDate | null = null

    private paymentType: PaymentType | null = null

    private notificationType: NotificationType | null = null

    private notificationAddress: NotificationAddress | null = null

    private frequency: Frequency | null = null

    private payAfterDeath = false

    private retryPeriod: RetryPeriod | null = null
    private senderID: SenderID | null = null
    private url: URL | null = null
    private senderNote: SenderNote | null = null

    public constructor(account: string | string[], private requestType: RequestType = RequestType.PaymentRequest) {
        this.account = this.validateAccount(account)
    }

    public getAccount(): BankAccountString[] {
        return this.account
    }

    public setAccount(account: string | string[]): QrRequest {
        const newRequest: QrRequest = cloneDeep(this)

        newRequest.account = this.validateAccount(account)

        return newRequest
    }

    private validateAccount(account: string | string[]): BankAccountString[] {
        if (isQrPayAccountString(account)) {
            return [account]
        }

        if (isQrPayAccountArray(account)) {
            return account
        }

        throw new Error(
            'Invalid account: please provide a string or array of strings (max 3), each containing IBAN or IBAN+BIC.',
        )
    }

    public getAmount(): AmountInCents | null {
        return this.amountInCents
    }

    public setAmountInCents(amountInCents: number | null): QrRequest {
        const newRequest: QrRequest = cloneDeep(this)

        newRequest.amountInCents = amountInCents === null ? null : this.validateAmount(amountInCents, 'CZK')

        return newRequest
    }

    private validateAmount(amount: number, currency: Dinero.Currency): AmountInCents {
        const dinero: Dinero.Dinero = Dinero({ amount: amount, currency: currency })

        if (!isQrPayAmountInCents(dinero)) {
            throw new Error('Invalid transfer amount in cents!')
        }

        return dinero
    }

    public getMessage(): Message | null {
        return this.message
    }

    public setMessage(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 60)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.message = value

            return newRequest
        } else {
            throw new Error('Invalid message')
        }
    }

    public getConstantSymbol(): ConstantSymbol | null {
        return this.constantSymbol
    }

    public setConstantSymbol(value: string | null): QrRequest {
        if (value === null || isValidNumericStringOfLength(value, 0, 10)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.constantSymbol = value

            return newRequest
        } else {
            throw new Error('Invalid constant symbol')
        }
    }

    public getSpecificSymbol(): SpecificSymbol | null {
        return this.specificSymbol
    }

    public setSpecificSymbol(value: string | null): QrRequest {
        if (value === null || isValidNumericStringOfLength(value, 0, 10)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.specificSymbol = value

            return newRequest
        } else {
            throw new Error('Invalid specific symbol')
        }
    }

    public getVariableSymbol(): VariableSymbol | null {
        return this.variableSymbol
    }

    public setVariableSymbol(value: string | null): QrRequest {
        if (value === null || isValidNumericStringOfLength(value, 0, 10)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.variableSymbol = value

            return newRequest
        } else {
            throw new Error('Invalid variable symbol')
        }
    }

    public getPaymentReference(): PaymentReference | null {
        return this.paymentReference
    }

    public setPaymentReference(value: string | null): QrRequest {
        if (value === null || isValidNumericStringOfLength(value, 0, 16)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.paymentReference = value

            return newRequest
        } else {
            throw new Error('Invalid payment reference')
        }
    }

    public getReceiverName(): ReceiverName | null {
        return this.receiverName
    }

    public setReceiverName(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 35)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.receiverName = value

            return newRequest
        } else {
            throw new Error('Invalid receiver name')
        }
    }

    public getDateOfTransaction(): Date | null {
        return this.dateOfTransaction?.date ?? null
    }

    public setDateOfTransaction(value: Date | null): QrRequest {
        const newRequest: QrRequest = cloneDeep(this)

        newRequest.dateOfTransaction = value === null ? null : new QrRequestDate(value)

        return newRequest
    }

    public getDateOfLastTransaction(): Date | null {
        return this.dateOfLastTransaction?.date ?? null
    }

    public setDateOfLastTransaction(value: Date | null): QrRequest {
        const newRequest: QrRequest = cloneDeep(this)

        newRequest.dateOfLastTransaction = value === null ? null : new QrRequestDate(value)

        return newRequest
    }

    public getPaymentType(): PaymentType | null {
        return this.paymentType
    }

    public setPaymentType(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 3)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.paymentType = value

            return newRequest
        } else {
            throw new Error('Invalid payment type')
        }
    }

    public getNotificationType(): NotificationType | null {
        return this.notificationType
    }

    public setNotificationType(value: NotificationType | null): QrRequest {
        const newRequest: QrRequest = cloneDeep(this)

        newRequest.notificationType = value

        return newRequest
    }

    public getNotificationAddress(): NotificationAddress | null {
        return this.notificationAddress
    }

    public setNotificationAddress(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 320)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.notificationAddress = value

            return newRequest
        } else {
            throw new Error('Invalid notification address!')
        }
    }

    public getFrequency(): Frequency | null {
        return this.frequency
    }

    public setFrequency(value: string | null): QrRequest {
        if (value === null || isValidFrequency(value)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.frequency = value

            return newRequest
        } else {
            throw new Error('Invalid frequency!')
        }
    }

    public isPayAfterDeath(): boolean {
        return this.payAfterDeath
    }

    public setPayAfterDeath(value: boolean): QrRequest {
        const newRequest: QrRequest = cloneDeep(this)

        newRequest.payAfterDeath = value

        return newRequest
    }

    public getRetryPeriod(): RetryPeriod | null {
        return this.retryPeriod
    }

    public setRetryPeriod(value: number | null): QrRequest {
        if (value === null || isValidRetryPeriod(value)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.retryPeriod = value

            return newRequest
        } else {
            throw new Error('Invalid retry period!')
        }
    }

    public getSenderID(): SenderID | null {
        return this.senderID
    }

    public setSenderID(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 20)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.senderID = value

            return newRequest
        } else {
            throw new Error('Invalid sender ID!')
        }
    }

    public getURL(): URL | null {
        return this.url
    }

    public setURL(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 140)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.url = value

            return newRequest
        } else {
            throw new Error('Invalid URL!')
        }
    }

    public getSenderNote(): SenderNote | null {
        return this.senderNote
    }

    public setSenderNote(value: string | null): QrRequest {
        if (value === null || isValidStringOfLength(value, 0, 60)) {
            const newRequest: QrRequest = cloneDeep(this)

            newRequest.senderNote = value

            return newRequest
        } else {
            throw new Error('Invalid sender note!')
        }
    }

    public toString(): string {
        return `${this.toStringWithoutChecksum()}*CRC32:${this.getChecksum()}`
    }

    public getChecksum(): string {
        return crc32(this.toStringWithoutChecksum()).toString(16).padStart(8, '0').toUpperCase()
    }

    private toStringWithoutChecksum(): string {
        return `${this.requestType}*${QrRequest.VERSION}*${this.getFields()
            .map<string>((field: [string, string]): string => `${field[0]}:${field[1]}`)
            .join('*')}`
    }

    private getFields(): Array<[string, string]> {
        return Array<[string, string | undefined | null | object]>(
            ['ACC', this.getAccount()[0]],
            ['AM', this.getAmount()?.toFormat('0.00')],
            ['CC', this.getAmount()?.getCurrency()],
            ['MSG', this.getMessage()],
            ['X-KS', this.getConstantSymbol()],
            ['X-SS', this.getSpecificSymbol()],
            ['X-VS', this.getVariableSymbol()],
            ['ALT-ACC', this.getAccount().length > 1 ? this.getAccount().slice(1).join(',') : null],
            ['RF', this.getPaymentReference()],
            ['RN', this.getReceiverName()],
            ['DT', this.dateOfTransaction],
            ['DL', this.dateOfLastTransaction],
            ['PT', this.getPaymentType()],
            ['NT', this.getNotificationType()],
            ['NTA', this.getNotificationAddress()],
            ['FRQ', this.getFrequency()],
            ['DH', this.isPayAfterDeath() ? '1' : null],
            ['X-PER', this.getRetryPeriod()],
            ['X-ID', this.getSenderID()],
            ['X-URL', this.getURL()],
            ['X-SELF', this.getSenderNote()],
        )
            .filter<[string, string | object]>(
                (value: [string, string | undefined | null | object]): value is [string, string | object] =>
                    value[1] !== null && value[1] !== undefined && value[1] !== '',
            )
            .map<[string, string]>((value: [string, string | object]): [string, string] => [
                value[0],
                sanitizeQrString(value[1]),
            ])
            .sort((a: [string, string], b: [string, string]): number => {
                if (a[0] < b[0]) {
                    return -1
                }

                if (a[0] > b[0]) {
                    return 1
                }

                if (a[1] < b[1]) {
                    return -1
                }

                if (a[1] > b[1]) {
                    return 1
                }

                return 0
            })
    }

    public isAlphanumericQRCode(): boolean {
        return /^[0-9A-Z $%*+\-./:]*$/.test(this.toStringWithoutChecksum())
    }
}

type BankAccountString = string & {
    readonly QrPayAccountString: unique symbol
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isQrPayAccountString = (value: any): value is BankAccountString => {
    if (typeof value !== 'string') {
        return false
    }

    const strParts: string[] = value.split('+')

    return (
        value.length > 0 &&
        value.length <= 46 &&
        strParts.length <= 2 &&
        isValidIBAN(strParts[0]) &&
        (strParts.length === 1 || isValidBIC(strParts[1]))
    )
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isQrPayAccountArray = (values: any): values is BankAccountString[] =>
    Array.isArray(values) &&
    values.length >= 1 &&
    values.length <= 3 &&
    values.reduce(
        (previousValue: boolean, value: string): boolean => isQrPayAccountString(value) && previousValue,
        true,
    )

type AmountInCents = Dinero.Dinero & {
    readonly Dinero: unique symbol
}

const isQrPayAmountInCents = (value: Dinero.Dinero): value is AmountInCents =>
    value.getAmount() > 0 && value.getAmount() < 10_000_000 * 100

type StringOfLength<Min, Max> = string & {
    min: Min
    max: Max
    readonly QrPayStringOfLength: unique symbol
}

const isValidStringOfLength = <Min extends number, Max extends number>(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    value: any,
    min: Min,
    max: Max,
): value is StringOfLength<Min, Max> => {
    if (typeof value !== 'string') {
        return false
    }

    const sanitizedValue: string = sanitizeQrString(value)

    return (
        sanitizedValue.length >= min &&
        sanitizedValue.length <= max &&
        !/[^\u0020-\u007e\u00a0-\u00ff]/g.test(value) &&
        !/^ /.test(value) &&
        !/ $/.test(value)
    )
}

type Message = StringOfLength<0, 60>

type NumericStringOfLength<Min, Max> = StringOfLength<Min, Max> & {
    readonly NumericStringOfLength: unique symbol
}

const isValidNumericStringOfLength = <Min extends number, Max extends number>(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    value: any,
    min: Min,
    max: Max,
): value is NumericStringOfLength<Min, Max> => isValidStringOfLength(value, min, max) && /^\d*$/.test(value)

type ConstantSymbol = NumericStringOfLength<0, 10>
type SpecificSymbol = NumericStringOfLength<0, 10>
type VariableSymbol = NumericStringOfLength<0, 10>

type PaymentReference = NumericStringOfLength<0, 16>

type ReceiverName = StringOfLength<0, 35>

type PaymentType = StringOfLength<0, 3>

type NotificationType = 'P' | 'E'
type NotificationAddress = StringOfLength<0, 320>

type Frequency = StringOfLength<1, 3> & {
    readonly Frequency: unique symbol
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isValidFrequency = (value: any): value is Frequency => /^\d{1,2}[DMY]$/.test(value)

type RetryPeriod = number & {
    readonly RetryPeriod: unique symbol
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isValidRetryPeriod = (value: any): value is RetryPeriod => typeof value === 'number' && value >= 0 && value <= 30

type SenderID = StringOfLength<0, 20>
type URL = StringOfLength<0, 140>
type SenderNote = StringOfLength<0, 60>
